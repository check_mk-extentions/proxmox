#!/bin/sh
cp package-info ~/var/check_mk/packages/proxmox
cp agent/proxmox ~/local/share/check_mk/agents/proxmox
cp checkman/proxmox ~/local/share/check_mk/checkman/
cp checks/proxmox-* ~/local/share/check_mk/checks/
